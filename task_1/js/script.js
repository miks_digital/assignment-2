var forms = document.getElementsByTagName('form');
var message = document.getElementById('message').value;

forms[0].addEventListener('submit', function (e) {
    e.preventDefault();

    alert(decrypt(message));

});


function decrypt(str) {
    return str.trim().split(" ").map(function(el) {
        return String.fromCharCode(parseInt(el, 2));
    }).join("")
}