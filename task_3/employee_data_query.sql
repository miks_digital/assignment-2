SELECT 
    E.id, 
    E.name, 
    E.dob AS birthdate,
    E.ssn,
    E.contact_info,
    ED.introduction, 
    ED.work_experience, 
    L.name AS language,
    M.name AS created_by,
    E.created_at,
    changed_by.name AS changed_by,
    E.changed_at    

FROM employee_data AS ED
    LEFT JOIN language AS L ON ED.language_id = L.id
    LEFT JOIN employee AS E ON E.id=ED.employee_id
    INNER JOIN manager AS M ON M.id=E.created_by_id
    INNER JOIN manager AS changed_by ON changed_by.id=E.changed_by_id
;
