<?php

require_once ('Calculator.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $response = [];

    $accepted_fields = [
        "car_value",
        "tax_value",
        "number_of_payments",
        "is_friday"
    ];

    $calculator = new Calculator($accepted_fields);

    $response['result'] = $calculator->calculate();
    $response['isOk'] = $calculator->getStatus();

    header('Content-Type: application/json');
    echo json_encode($response);
}

