<?php


class Calculator
{
    const BASE_PRICE = 11;
    const FRIDAY_PRICE = 13;
    const COMMISSION = 17;

    private $accepted_fields = [];
    private $isOk = true;
    private $inputData = [];
    private $base_price = 0;
    private $commission = 0;
    private $priceRatio = 0;
    private $tax_value;

    public function __construct($input_keys = [])
    {
        $this->accepted_fields = $input_keys;
    }

    /**
     * @return array
     */
    public function calculate()
    {
        $result = [];

        $this->inputData = $this->validateInput();

        if($this->isOk && !empty($this->inputData)) {
            $result['base_price']   = round($this->calculateBasePrice(),2);
            $result['commission']   = round($this->calculateCommission(),2);
            $result['tax']          = round($this->calculateTax(),2);
            $result['tax_choosen']  = $this->inputData['tax_value'];
            $result['total_cost']   = $result['base_price'] + $result['commission'] + $result['tax'];
            $result['number_of_payments'] = (int)$this->inputData['number_of_payments'];
            $result['price_ratio'] = $this->priceRatio;
            $result['car_value'] = $this->inputData['car_value'];
            $result['installments'] = [
                'base_price' => $this->handleInstallments($result['base_price']),
                'comission'  => $this->handleInstallments($result['commission']),
                'tax'        => $this->handleInstallments($result['tax']),
                'total_cost' => $this->handleInstallments($result['total_cost']),
            ];
        }
        return $result;
    }

    /**
     * @param $total
     * @return array
     */
    private function handleInstallments($total)
    {
        $installments = [];

        $number_of_payments = $this->inputData['number_of_payments'];
        $each = $total / $number_of_payments;
//        $each =  intdiv($total, $number_of_payments);
        $reminder = $total - $each * $number_of_payments;

        do{
            array_push($installments, round($each, 2));
            $number_of_payments--;
        }
        while($number_of_payments >= 0);

//        $installments[0] = $installments[0] + round($reminder, 2);

        // still have no idea how to handle cases when sums does not divide equally ...
        // some rows can be handled but at the same time other rows go wrong
        // need more time to find acceptable solution

        return $installments;
    }

    /**
     * @return array
     */
    public function validateInput()
    {
        $data = [];

        // some basic validation and filtering
        // for example, check if all required fields are submitted
        foreach ($this->accepted_fields as $input_field) {
            if (!filter_has_var(INPUT_POST, $input_field)) {
                $this->isOk = false;
            } else {
                $data[$input_field] = filter_input(INPUT_POST, $input_field);
            }
        }

        return $data;
    }

    /**
     * @return float|int
     */
    private function calculateBasePrice()
    {
        $this->priceRatio = (int)$this->inputData['is_friday'] === 1 ? self::FRIDAY_PRICE : self::BASE_PRICE;

        $this->base_price = $this->inputData['car_value'] / 100 * $this->priceRatio;
        return $this->base_price;
    }

    /**
     * @return float|int
     */
    private function calculateCommission()
    {
        $this->commission = $this->base_price / 100 * self::COMMISSION;
        return $this->commission;
    }

    private function calculateTax()
    {
        $this->tax_value = $this->base_price / 100 * $this->inputData['tax_value'];
        return $this->tax_value;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->isOk;
    }
}