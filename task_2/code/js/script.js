var forms = document.getElementsByTagName('form');

var carValueInput = document.getElementById("car_value_slider");
var carValue = document.getElementById("car_value");

var taxValueInput = document.getElementById("tax_value_slider");
var taxValue = document.getElementById("tax_value");

var paymentsCountInput = document.getElementById("count_payments_value_slider");
var paymentsCountValue = document.getElementById("count_payments_value");

carValue.innerText = carValueInput.value;
taxValue.innerText = taxValueInput.value;
paymentsCountValue.innerText = paymentsCountInput.value;

carValueInput.addEventListener('input', function () {
    carValue.innerText = this.value;
});

taxValueInput.addEventListener('input', function () {
    taxValue.innerText = this.value;
});

paymentsCountInput.addEventListener('input', function () {
    paymentsCountValue.innerText = this.value;
});

forms[0].addEventListener('submit', function (e) {
    e.preventDefault();

    var date = new Date();
    var isFriday = 0;

    // special Friday hours check
    if (date.getDay() === 5 && (date.getHours() > 15 && date.getHours() < 20)) {
        isFriday = 1;
    }

    var xhr = new XMLHttpRequest();
    var data = new FormData();

    data.append("car_value", carValueInput.value);
    data.append("tax_value", taxValueInput.value);
    data.append("number_of_payments", paymentsCountInput.value);
    data.append("is_friday", isFriday);

    xhr.open("POST", "/backend/api.php", true);
    xhr.send(data);

    xhr.onload = function() {
        if (xhr.status != 200) {
            console.error(`Error ${xhr.status}: ${xhr.statusText}`);
        } else {
            console.log(xhr.response);

            var data=xhr.responseText;
            var jsonResponse = JSON.parse(data);

            if(jsonResponse.isOk) {
                var table = document.getElementById("calculation_result");
                var price_ratio = document.getElementById("price_ratio");
                var tax_choosen = document.getElementById("tax_choosen");
                var value = document.getElementById("value");
                var base_premium = document.getElementById("base_premium");
                var commission = document.getElementById("commission");
                var tax = document.getElementById("tax");
                var total_cost = document.getElementById("total_cost");
                var base_premium_1 = document.getElementById("base_premium_1");
                var commission_1 = document.getElementById("commission_1");
                var tax_1 = document.getElementById("tax_1");
                var total_cost_1 = document.getElementById("total_cost_1");
                var base_premium_other = document.getElementById("base_premium_other");
                var commission_other = document.getElementById("commission_other");
                var tax_other = document.getElementById("tax_other");
                var total_cost_other = document.getElementById("total_cost_other");

                price_ratio.innerText = jsonResponse.result.price_ratio;
                tax_choosen.innerText = jsonResponse.result.tax_choosen;
                value.innerText = jsonResponse.result.car_value;

                base_premium.innerText = jsonResponse.result.base_price.toFixed(2);
                commission.innerText = jsonResponse.result.commission.toFixed(2);
                tax.innerText = jsonResponse.result.tax.toFixed(2);
                total_cost.innerText = jsonResponse.result.total_cost.toFixed(2);


                base_premium_1.innerText = jsonResponse.result.installments.base_price[0].toFixed(2);
                commission_1.innerText = jsonResponse.result.installments.comission[0].toFixed(2);
                tax_1.innerText = jsonResponse.result.installments.tax[0].toFixed(2);
                total_cost_1.innerText = jsonResponse.result.installments.total_cost[0].toFixed(2);

                base_premium_other.innerText = jsonResponse.result.installments.base_price[1].toFixed(2);
                commission_other.innerText = jsonResponse.result.installments.comission[1].toFixed(2);
                tax_other.innerText = jsonResponse.result.installments.tax[1].toFixed(2);
                total_cost_other.innerText = jsonResponse.result.installments.total_cost[1].toFixed(2);

                table.style.display = 'block';
            }
        }
    };
});
