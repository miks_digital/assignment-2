### Docker setup with php and nginx

To spin up containers run from **task_2** folder
```bash
docker-compose up -d
```
Navigate to http://0.0.0.0:8080/ to see the result

![screenshot](screenshot.png)
